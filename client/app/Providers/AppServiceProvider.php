<?php

namespace App\Providers;

use DannyVerpoort\CatsApi\Api\BreedsApi;
use DannyVerpoort\CatsApi\Api\ImageApi;
use DannyVerpoort\CatsApi\Configuration;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BreedsApi::class, function() {
            return new BreedsApi(null, $this->getConfiguration());
        });
        $this->app->bind(ImageApi::class, function() {
            return new ImageApi(null, $this->getConfiguration());
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * @return Configuration
     */
    private function getConfiguration(): Configuration
    {
        $configuration = new Configuration();
        $configuration->setHost(env('API_HOST', $configuration->getHost()));
        return $configuration;
    }
}
