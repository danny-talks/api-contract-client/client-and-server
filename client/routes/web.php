<?php

use DannyVerpoort\CatsApi\Api\BreedsApi;
use DannyVerpoort\CatsApi\Api\ImageApi;
use DannyVerpoort\CatsApi\ApiException as CatsApiException;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (BreedsApi $breedsApi, ImageApi $imageApi) {

    $breeds = [];
    $fallbackImage = 'https://via.placeholder.com/200?text=No+Image';

    try {
        $breeds = $breedsApi->breedsGet(null, 10);
    } catch (CatsApiException $exception) {
        dump($exception);
    }


    return view('welcome', ['breeds' => $breeds, 'fallbackImage' => $fallbackImage]);
});
