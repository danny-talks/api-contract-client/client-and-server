# Client and Server
This repository contains both the API and the client consuming that API. The client makes use of the package generated in [this](https://gitlab.com/danny-talks/api-contract-client/client-packages) repository.

## Prerequisites
This project requires docker and docker-compose to run. Please follow the instructions [here](https://docs.docker.com/compose/install/) to install it.

## Getting started
* Clone the repository using `git clone git@gitlab.com:danny-talks/api-contract-client/client-and-server.git`  (or use your favourite git tool)
* Go into the repository `cd client-and-server`
* Run `docker-compose up -d` and wait for the process to complete
  * The client can be accessed at http://localhost:8080
  * The server can be accessed at http://localhost:8080/api/*
  * The API contract can be accessed at http://localhost:8080/api-docs